unit ControllerThread;
{$mode objfpc}
interface

uses 
{$IFDEF UNIX} 
cthreads,cmem,
{$ENDIF}
Controller,Classes,KasseThreads,Generator;

type
  TControllerThread = class(TThread)
     protected
     procedure Execute; override;
     public
     Controller:TController;
     constructor Create(CreateSuspended:boolean;const StackSize:SizeUInt = DefaultStackSize);
     destructor Destroy; override;
  end;

implementation

procedure TControllerThread.Execute; 
begin
  Controller.Kassen.AddKasse;
  Controller.Kassen.AddKasse;
  Controller.Generator.GeneratePeople;
  Controller.PeopleToKassen;
  while true do
        begin
          Controller.Generator.GeneratePeople;
          Controller.PeopleToKassen;
          Controller.CheckQueue;
          Controller.LoadData;
        end;
end;

constructor TControllerThread.Create(CreateSuspended:boolean;const StackSize:SizeUInt = DefaultStackSize);
begin
  inherited Create(CreateSuspended,StackSize);
  FreeOnTerminate:=false;
  Controller:=TController.Create;
end;

destructor TControllerThread.Destroy;
begin
  Controller.Generator.Destroy;
  Controller.Kassen.Free;
  Controller.Destroy;
  inherited Destroy;
end;

end.
