unit Generator;
{$mode objfpc}
interface
uses fpTimer, SysUtils, DateUtils;

type
TGenerator = class
   protected
   CanGenerate:boolean;
   NextGeneration: TDateTime;
   public
   GeneratedPeople: integer;
   constructor Create;
   procedure GeneratePeople;
   destructor Destroy; override;
end;

implementation

constructor TGenerator.Create;
begin
  inherited Create;
  Randomize;
  CanGenerate:=true;
  GeneratedPeople:=0;
end;

procedure TGenerator.GeneratePeople;
{Speichert generierte Menschen in TGenerator.GeneratedPeople}
var x:integer;
  Hour,Minute,Seconds,Milliseconds:word;
begin
if CompareDateTime(NextGeneration,Time)<0 then CanGenerate:=true;
if not CanGenerate then exit;
CanGenerate:=false;
x:=Random(10);
GeneratedPeople:=GeneratedPeople+x+1;
x:=(Random(10)+5);
DecodeTime(Now,Hour,Minute,Seconds,Milliseconds);
if (Seconds+x)>59 then
   begin
     Seconds:=Seconds+x-60;
     Inc(Minute);
     if Minute>59 then
        begin
          Minute:=0;
          Inc(Hour);
        end;
   end
else
   begin
     Seconds:=Seconds+x;
   end;
NextGeneration:=EncodeTime(Hour,Minute,Seconds,MilliSeconds);
end;

destructor TGenerator.Destroy;
begin
  inherited Destroy;
end;
   
end.
