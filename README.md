### Was ist das ###

* Das ist ein Projekt, in dem ich versuchte, das Leben eines Supermarktes zu simulieren. Es gibt Kassen, die den Kunden bedienen. Kunden kommen und kommen. Wenn es zu viele Kunden gibt, werden die neuen Kassen geöffnet. Wenn es zu wenig gibt, schließen sich die Kassen. In der Quellcode gibt es ein Klassendiagramm, in dem man die Architektur des Programms sehen kann.

### What is this repository for? ###

* It's my project, that simulates a life in a supermarket. There are several cash offices, that open or close, when there are too many/few customers. In a repository I have a classes diagramm.