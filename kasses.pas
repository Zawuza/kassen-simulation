unit Kasses;
{$mode objfpc}
interface

uses SmallPeople,crt,SysUtils;

type TKasse = class
   People: TPeople;
   constructor Create;
   procedure Serve;
   destructor Destroy; override;
end;

implementation

{TKasse}

constructor TKasse.Create;
begin
  People:=TPeople.Create;
end;

procedure TKasse.Serve;
var DelayTime:integer;
begin
  if People.Count=0 then exit;
  DelayTime:=People.First.ServingTime;
  Delay(DelayTime);
  People.Remove(People.First);  
end;

destructor TKasse.Destroy;
begin
  People.Free;
  inherited Destroy;
end;

end.
