unit SmallPeople;
{$mode objfpc}
interface
uses fgl;
type 
TPerson = class
      ServingTime: integer;
      constructor Create;
   end;

TSpecialList = specialize TFPGObjectList<TPerson>;

TPeople = class(TSpecialList)
  procedure AddAPerson;
end;

implementation

{TPerson}

constructor TPerson.Create;
begin
  inherited Create;
  ServingTime:=Random(5000);
end;

{TPeoples}

procedure TPeople.AddAPerson;
var Person:TPerson;
begin
  Person:=TPerson.Create;
  Add(Person);
end;
   
end.

