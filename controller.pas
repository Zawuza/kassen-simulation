unit Controller;
{$mode objfpc}
interface
uses KasseThreads,Generator;

type
TController = class
   Kassen: TKassen;
   Generator: TGenerator;
   MaxQueueLength: integer;
   MinQueueLength: integer;
   Data: array [0..100] of integer; {array for drawing: -1 -> kasse doesn't exist}
   constructor Create;
   procedure PeopleToKassen;
   procedure CheckQueue;
   procedure LoadData;
   destructor Destroy; override;
end;

implementation

constructor TController.Create;
var i:integer;
begin
  inherited Create;
  MaxQueueLength:=10;
  MinQueueLength:=3;
  Kassen:=TKassen.Create(false {Own objects});
  Generator:=TGenerator.Create;
  for i:=0 to Length(Data) do
  begin
    Data[i]:=-1;
  end;
end;

procedure TController.PeopleToKassen;
var People, i, j, minpeoplecount, indexofmin:integer;
begin
   People:=Generator.GeneratedPeople;
   if People=0 then exit;
   Generator.GeneratedPeople:=0;
   if Kassen.Count=0 then exit;
   for i:=1 to People do
      begin
      minpeoplecount:=High(minpeoplecount);
         for j:=0 to Kassen.Count-1 do
            begin
                if Kassen.Items[j].Kasse.People.Count<minpeoplecount then
                  begin
                    minpeoplecount:=Kassen.Items[j].Kasse.People.Count;
                    indexofmin:=j;
                  end;
            end;
      Kassen[indexofmin].Kasse.People.AddAPerson;
      end;
end;

procedure TController.CheckQueue;
var i:integer;
    Queue:integer;
begin
   for i:=0 to Kassen.Count-1 do
      begin
        if i>Kassen.Count-1 then exit; {Fehler: List - Out Of Bounds}
        if Kassen.Items[i].Kasse.People.Count>MaxQueueLength then
          begin
             Queue:=Kassen.Items[i].Kasse.People.Count;
             Kassen.AddKasse;
             Kassen[i].Kasse.People.Clear;
             Generator.GeneratedPeople:=Generator.GeneratedPeople+Queue;
             PeopleToKassen;
          end;
        if (Kassen.Items[i].Kasse.People.Count<MinQueueLength)
            and
           (Kassen.Count>1) then
          begin
             Queue:=Kassen[Kassen.Count-1].Kasse.People.Count;
             Generator.GeneratedPeople:=Generator.GeneratedPeople+Queue;
             Kassen.DeleteKasse;
             PeopleToKassen;
             Data[i]:=-1;
          end;
      end;
end;

procedure TController.LoadData;
var i:integer;
begin
   for i:=0 to Kassen.Count-1 do
      begin
         Data[i]:=Kassen.Items[i].Kasse.People.Count;
      end;
   for i:=Kassen.Count to 100 do
     begin
        Data[i]:=-1;
     end;
end;

destructor TController.Destroy;
begin
   Kassen.Free;
   Generator.Free;
   inherited Destroy;
end;

end.
   
