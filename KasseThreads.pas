unit KasseThreads;
{$mode objfpc}
interface
uses
{$IFDEF UNIX} 
cthreads,cmem,
{$ENDIF}
Kasses,Classes, fgl;

type
TKasseThread = class (TThread)
   protected
      procedure Execute; override;
   public
      Kasse:TKasse;
      constructor Create(CreateSuspended:boolean; const StackSize:SizeUInt = DefaultStackSize);
   end;
   
TSpecializeKassenList = specialize TFPGObjectList<TKasseThread>;  
 
TKassen = class(TSpecializeKassenList)
   public
   procedure AddKasse;
   procedure DeleteKasse;
end;
   
implementation

{TKasseThread}

procedure TKasseThread.Execute; 
begin
  while true do
    begin
      Kasse.Serve;
    end;
end;

constructor TKasseThread.Create(CreateSuspended:boolean; const StackSize:SizeUInt = DefaultStackSize);
begin
  inherited Create(CreateSuspended,StackSize);
  Kasse:=TKasse.Create;
end;

{TKassen}

procedure TKassen.AddKasse;
var NewKasse: TKasseThread;
begin
NewKasse:=TKasseThread.Create(true);
Add(NewKasse);  
NewKasse.Resume;
end;

procedure TKassen.DeleteKasse;
begin
Remove(Items[Count-1]);
end;
   
end.
